SHELL := /bin/bash
export PYTHONUNBUFFERED := 1
BASE := $(shell realpath $$(/bin/pwd))
export PATH := $(shell echo $$PATH):$(BASE)/bin

PYTHON_GLOBAL := $(shell /usr/bin/which python{3.8,3.7} | head -n1)

ifneq (,$(findstring 3.8,$(PYTHON_GLOBAL)))
    PY_VERSION := 3.8
endif
ifneq (,$(findstring 3.7,$(PYTHON_GLOBAL)))
    PY_VERSION := 3.7
endif

VENV_DIRNAME := .venv
VENV := $(BASE)/$(VENV_DIRNAME)

# The venv `python` executable.
PYTHON := $(VENV)/bin/python$(PY_VERSION)

# This is the command we use as `pip`. As this guy explains nicely, we don't use
# the `pip` binary directly:
# https://snarky.ca/why-you-should-use-python-m-pip/
PIP_COMMAND := cd $(BASE)/ && $(PYTHON) -m pip
# This is the `pip` executable, which we need for `make` to see if it exists.
PIP := $(VENV)/bin/pip$(PY_VERSION)
export MYPYPATH := $(BASE)

.DEFAULT_GOAL := build


$(PIP): var VERSION ITERATION
	@# as per `pyramid-cookiecutter-alchemy`: `$(PIP_COMMAND) install --upgrade`
	[ -f $(PIP) ] || \
	( \
		$(PYTHON_GLOBAL) -m venv $(VENV); \
		$(PIP_COMMAND) install --upgrade --no-deps \
			pip \
			wheel \
			setuptools; \
	)


.PHONY: build
build: $(PIP) requirements.txt
	# for `brew` installed libxml2 to be found, we need some env vars
	__EXTRA__=$(if $(__EXTRA__),$(__EXTRA__),'') \
	LDFLAGS="-L/usr/local/opt/libxml2/lib" \
	CPPFLAGS="-I/usr/local/opt/libxml2/include" \
	PKG_CONFIG_PATH="/usr/local/opt/libxml2/lib/pkgconfig" \
		$(PIP_COMMAND) \
			--isolated \
			--disable-pip-version-check \
			install -r $(BASE)/requirements.txt $(__EXTRA__)
	cd $(VENV)/bin/ && ln -sfn python$(PY_VERSION) python


.PHONY: dev.build
dev.build: $(PIP) requirements.development.txt
	__EXTRA__=$(if $(__EXTRA__),$(__EXTRA__),'') \
	$(PIP_COMMAND) \
		--isolated \
		--disable-pip-version-check \
		install -r $(BASE)/requirements.development.txt $(__EXTRA__)


.PHONY: mypy
mypy:
	#   :$(VENV)/lib/python3.6/site-packages
	"$(VENV)/bin/mypy" \
		--python-version $(PY_VERSION) \
		--python-executable "$(PYTHON)" \
		--ignore-missing-imports \
		--config-file "$(BASE)/mypy.ini" \
		.

.PHONY: test
test: mypy ITERATION VERSION
	ENV=$(if $(ENV),$(ENV),'test') "$(VENV)/bin/py.test"

.PHONY: dev.test
dev.test: bin/chromedriver mypy ITERATION VERSION
	ENV=$(if $(ENV),$(ENV),'test') "$(VENV)/bin/py.test" --maxfail=1


# only for initial build, without having a requirements file in place
.PHONY: build.setup
build.setup: $(PIP)
	# for `brew` installed libxml2 to be found, we need some env vars
	LDFLAGS="-L/usr/local/opt/libxml2/lib" \
	CPPFLAGS="-I/usr/local/opt/libxml2/include" \
	PKG_CONFIG_PATH="/usr/local/opt/libxml2/lib/pkgconfig" \
		$(PIP_COMMAND) \
			--isolated \
			--disable-pip-version-check \
			install -e .[standalone]

.PHONY: requirements.txt
requirements.txt: $(PIP)
	@# as `make` will take the timestamp into account, we have to make our own "exist-only" check
	[ -f requirements.txt ] || \
	( \
		make build.setup && cat \
			<($(PIP_COMMAND) freeze \
				| grep -v pansen \
				) \
			\
			<(echo '-e .[standalone]') \
			> requirements.txt \
	)

# `pip freeze` only for production packages
.PHONY: pip.freeze
pip.freeze:
	rm -f requirements.txt
	@make clean
	make build

# only for initial build, without having `requirements.development.txt` file in place
.PHONY: dev.build.setup
dev.build.setup: $(PIP)
	# for `brew` installed libxml2 to be found, we need some env vars
	LDFLAGS="-L/usr/local/opt/libxml2/lib" \
	CPPFLAGS="-I/usr/local/opt/libxml2/include" \
	PKG_CONFIG_PATH="/usr/local/opt/libxml2/lib/pkgconfig" \
		$(PIP_COMMAND) \
			--isolated \
			--disable-pip-version-check \
			install -e .[testing]

# `pip freeze` for production and development packages
.PHONY: dev.pip.freeze
dev.pip.freeze: pip.freeze
	rm -f requirements.development.txt
	# clean happened before in `pip.freeze`
	make dev.build

requirements.development.txt: requirements.txt
	[ -f requirements.development.txt ] || \
	( \
		make dev.build.setup; \
		cat <($(PIP_COMMAND) freeze \
				| grep -v pansen \
				) \
			<(echo '-e .[testing]') \
			> .requirements.development.txt; \
		cat \
			<(echo '-r requirements.txt') \
			<(grep -v -f requirements.txt .requirements.development.txt) \
			\
			<(echo '-e .[testing]') \
			> requirements.development.txt; \
		rm .requirements.development.txt \
	)


bin:
	mkdir -p "$(BASE)/bin"

var:
	mkdir -p "$(BASE)/var/log/webapp" "$(BASE)/var/run/webapp" "$(BASE)/var/transfer" "$(BASE)/var/uploads"

ITERATION:
	./bin/make_iteration

VERSION:
	./bin/make_version


.PHONY: clean
clean: pyc-clean
	rm -rf \
		"$(VENV)" \
		"$(BASE)/.mypy_cache" \
		./*".egg-info"

.PHONY: pyc-clean
pyc-clean:
	@find ./ -type d -name __pycache__ | xargs -P 20 rm -rf
	@find ./ -name '*.pyc'             | xargs -P 20 rm -rf

